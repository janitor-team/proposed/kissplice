option(
    USER_GUIDE
    "compile and install a pdf user_guide ; requires pdflatex and standard latex packages"
    ON
)
if (USER_GUIDE)
    find_package(LATEX)
    if(NOT PDFLATEX_COMPILER)
        message(FATAL_ERROR "pdflatex not found, install latex or disable user_guide generation with -DUSER_GUIDE=OFF")
    endif()

    # add version number
    configure_file("${CMAKE_CURRENT_SOURCE_DIR}/user_guide.in.tex" "${CMAKE_CURRENT_BINARY_DIR}/user_guide.tex" @ONLY)

    # Call pdflatex twice to fix references.
    # Called with pwd=source_dir to have access to pictures, but -output-directory=binary_dir to prevent clutter.
    # -halt-on-error -interaction=nonstopmode are here to prevent any user prompt and have a clean stop+error instead.
    # The quiet.sh script tries to remove verbosity if VERBOSE make is not requested.
    add_custom_command(
        OUTPUT "${CMAKE_CURRENT_BINARY_DIR}/user_guide.pdf"
        COMMAND "${CMAKE_CURRENT_SOURCE_DIR}/quiet.sh" "${PDFLATEX_COMPILER}" -halt-on-error -interaction=nonstopmode "-output-directory=${CMAKE_CURRENT_BINARY_DIR}" "${CMAKE_CURRENT_BINARY_DIR}/user_guide.tex"
        COMMAND "${CMAKE_CURRENT_SOURCE_DIR}/quiet.sh" "${PDFLATEX_COMPILER}" -halt-on-error -interaction=nonstopmode "-output-directory=${CMAKE_CURRENT_BINARY_DIR}" "${CMAKE_CURRENT_BINARY_DIR}/user_guide.tex"
        COMMAND "${CMAKE_CURRENT_SOURCE_DIR}/rm_pdf_id.sh" "${CMAKE_CURRENT_BINARY_DIR}/user_guide.pdf"
        WORKING_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}"
        MAIN_DEPENDENCY "${CMAKE_CURRENT_BINARY_DIR}/user_guide.tex"
        DEPENDS "${CMAKE_CURRENT_SOURCE_DIR}/quiet.sh" "${CMAKE_CURRENT_SOURCE_DIR}/rm_pdf_id.sh"
    )
    add_custom_target(doc ALL DEPENDS "${CMAKE_CURRENT_BINARY_DIR}/user_guide.pdf")
    install(FILES "${CMAKE_CURRENT_BINARY_DIR}/user_guide.pdf" DESTINATION "${CMAKE_INSTALL_DOCDIR}")
else()
    message(WARNING "user_guide.pdf will not be built, disabled by USER_GUIDE variable")
endif()
