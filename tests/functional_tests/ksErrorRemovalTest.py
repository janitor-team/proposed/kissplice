#!/usr/bin/env python3
import re
import os
import fnmatch
from sys import argv
from os.path import dirname, abspath
import ProcessLauncher
import shutil 
TEST_INSTDIR=dirname(abspath(argv[0])) 

  

command_line= argv[1]+"/ks_error_removal "+TEST_INSTDIR+"/data/graph_HBM75brainliver_100000_k25.edges "+TEST_INSTDIR+"/data/graph_HBM75brainliver_100000_k25.abundance 0.02"+TEST_INSTDIR+"/data/test_ks_error_removal_output"
result = ProcessLauncher.run(command_line)
print(result)

#removing the output files
for root,dirs,files in os.walk(TEST_INSTDIR+"/data"):
    for filename in fnmatch.filter(files,'test_ks_error_removal_output*'):
        os.remove(os.path.join(root,filename))

# testing expected results
successful = True
if not (re.search('252 out of 5336 edges removed', result.decode())):
    successful = False
 
# summary
if successful:
    print("ks_error_removal: test SUCCESSFUL")
else:
    print("ks_error_removal: test FAILED")