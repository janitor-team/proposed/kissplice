#!/usr/bin/env python3
import re
import os
import fnmatch
from sys import argv
from os.path import dirname, abspath
import ProcessLauncher
TEST_INSTDIR=dirname(abspath(argv[0])) 

command_line= argv[1]+"/ks_bubble_enumeration " +TEST_INSTDIR+"/data/graph_info_bcc "+TEST_INSTDIR+"/data/graph_contents_edges_bcc "+TEST_INSTDIR+"/data/graph_contents_nodes_bcc "+TEST_INSTDIR+"/data/graph_all_edges_bcc "+TEST_INSTDIR+"/data/graph_all_nodes_bcc 2 25 "+TEST_INSTDIR+"/data/test_ks_bubble_enumeration_output 3 bcc_test 0 -u 1000 -L 49 -l 42 -M 10000 -b 5"

result = ProcessLauncher.run(command_line)
print(result)

# testing expected results
successful = True
if not (re.search('No of bubbles: 4', result.decode())):
    successful = False
    
# #removing the output files
for root,dirs,files in os.walk(TEST_INSTDIR+"/data"):
    for filename in fnmatch.filter(files,'test_ks_bubble_enumeration_output*'):
        os.remove(os.path.join(root,filename))

# summary
if successful:
    print("Ks_Bubble_Enumeration_Test: test SUCCESSFUL")
else:
    print("Ks_Bubble_Enumeration_Test: test FAILED")
