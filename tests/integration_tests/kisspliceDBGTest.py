#!/usr/bin/env python3
from sys import argv
from os.path import dirname, abspath
import filecmp
import shutil
import shlex
import subprocess

TEST_INSTDIR = dirname(abspath(argv[0]))
BINDIR = argv[1]

result = subprocess.run(
    shlex.split(f"{BINDIR}/kissplice -k 25 -M 1000 -r {TEST_INSTDIR}/data/HBM75brain_100000.fasta -r {TEST_INSTDIR}/data/HBM75liver_100000.fasta --keep-counts -o {TEST_INSTDIR}/results"),
    check = True, capture_output = True
)
assert result.stderr == b""

# Extract entries at column_index, sort them and store them in file+"0"
def copy_sorted_entries(column_index: int, file: str):
    with open(file) as lines:
        entries = [l.strip().split()[column_index] for l in lines]
    entries.sort()
    with open(file + "0", "w") as f:
        f.writelines(entry + "\n" for entry in entries)

copy_sorted_entries(2, f"{TEST_INSTDIR}/results/graph_HBM75brain_100000_HBM75liver_100000_k25.edges")
copy_sorted_entries(1, f"{TEST_INSTDIR}/results/graph_HBM75brain_100000_HBM75liver_100000_k25.nodes")
copy_sorted_entries(0, f"{TEST_INSTDIR}/results/graph_HBM75brain_100000_HBM75liver_100000_k25.abundance")

assert filecmp.cmp(f"{TEST_INSTDIR}/results/graph_HBM75brain_100000_HBM75liver_100000_k25.edges0", f"{TEST_INSTDIR}/data/graph_HBM75brain_100000_HBM75liver_100000_k25.edges0")
assert filecmp.cmp(f"{TEST_INSTDIR}/results/graph_HBM75brain_100000_HBM75liver_100000_k25.nodes0", f"{TEST_INSTDIR}/data/graph_HBM75brain_100000_HBM75liver_100000_k25.nodes0")
assert filecmp.cmp(f"{TEST_INSTDIR}/results/graph_HBM75brain_100000_HBM75liver_100000_k25.abundance0", f"{TEST_INSTDIR}/data/graph_HBM75brain_100000_HBM75liver_100000_k25.abundance0")
    
shutil.rmtree(f"{TEST_INSTDIR}/results")