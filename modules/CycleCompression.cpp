/* ***************************************************************************
 *
 *                              KisSplice
 *      de-novo calling alternative splicing events from RNA-seq data.
 *
 * ***************************************************************************
 *
 * Copyright INRIA
 *  contributors :  Vincent Lacroix
 *                  Pierre Peterlongo
 *                  Gustavo Sacomoto
 *                  Vincent Miele
 *                  Alice Julien-Laferriere
 *                  David Parsons
 *
 * pierre.peterlongo@inria.fr
 * vincent.lacroix@univ-lyon1.fr
 *
 * This software is a computer program whose purpose is to detect alternative
 * splicing events from RNA-seq data.
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".

 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.

 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */


#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <list>
#include <utility>
#include "debug.h"
#include "NGraph.h"
#include "Utils.h"
#include "CycleCompression.h"

#define MAX 1024
//MAX2 and MIN2 never used? TODO: Remove?
#define MAX2(a,b) ((a) > (b) ? (a) : (b)) 
#define MIN2(a,b) ((a) < (b) ? (a) : (b)) 
using namespace std;

idx_dir reverse_dir(idx_dir node)
{
  return make_pair(node.first, reverse_dir(node.second));
}

bool is_same_node( const  idx_dir u, const idx_dir v)
{
  return (v.first == u.first && v.second == u.second);
}


/*!
 * \brief Merge sequence information between two nodes and print it in the snp file
 * with bbc number, cycle and length of upper and lower path.
 * Returns the merged sequence
 *  \param seq1 sequence of the upper path
 *  \param seq2 sequence of the lower path
 *  \param snp_log_file the file to be printing the two paths
 *  \param bccid id of the bi-connected component being treated
 */
string merge_sequences( const string seq1, const string seq2 )
{
  string merged;
  for (int i = 0 ; i < (int)seq1.size(); i++)
    merged += (seq1[i] == seq2[i] ? seq1[i] : 'N');

  return merged;
}

void output_sequences( string seq1, string seq2, FILE * snp_log_file, const int bccid, const int cycleNum, const int contextL, const int contextR, const int kValue)
{
  int u_len = (int)seq1.size();
  int l_len = (int)seq2.size();
  if( contextL != 0 || contextR !=0 )
  {
  seq1 = toLowerContext(seq1, contextL, contextR);
  seq2 = toLowerContext(seq2, contextL, contextR);
  }
  if (u_len- (contextL + contextR) > 2* kValue +1) // multiple SNP
  {
    fprintf(snp_log_file, ">bcc_%d|Cycle_%d|Type_0b|upper_path_Length_%d\n", bccid, cycleNum, u_len- (contextL + contextR));
    fprintf(snp_log_file, "%s\n", seq1.c_str());
    fprintf(snp_log_file, ">bcc_%d|Cycle_%d|Type_0b|lower_path_Length_%d\n", bccid,  cycleNum, l_len- (contextL + contextR));
    fprintf(snp_log_file, "%s\n", seq2.c_str());
  }
  else // single SNP
  {
    fprintf(snp_log_file, ">bcc_%d|Cycle_%d|Type_0a|upper_path_Length_%d\n", bccid, cycleNum, u_len- (contextL + contextR));
    fprintf(snp_log_file, "%s\n", seq1.c_str());
    fprintf(snp_log_file, ">bcc_%d|Cycle_%d|Type_0a|lower_path_Length_%d\n", bccid,  cycleNum, l_len- (contextL + contextR));
    fprintf(snp_log_file, "%s\n", seq2.c_str());
  }

}
