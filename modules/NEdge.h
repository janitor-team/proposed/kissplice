/* ***************************************************************************
 *
 *                              KisSplice
 *      de-novo calling alternative splicing events from RNA-seq data.
 *
 * ***************************************************************************
 *
 * Copyright INRIA 
 *  contributors :  Vincent Lacroix
 *                  Pierre Peterlongo
 *                  Gustavo Sacomoto
 *                  Vincent Miele
 *                  Alice Julien-Laferriere
 *                  David Parsons
 *
 * pierre.peterlongo@inria.fr
 * vincent.lacroix@univ-lyon1.fr
 *
 * This software is a computer program whose purpose is to detect alternative
 * splicing events from RNA-seq data.
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".

 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.

 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
 
 
 
 
 
#ifndef NEDGE_H
#define NEDGE_H


// ===========================================================================
//                               Include Libraries
// ===========================================================================
#include <cstdio>
#include <cstdlib>
#include <string>

// ===========================================================================
//                             Include Project Files
// ===========================================================================
#include "Utils.h"

/* The node class is used into the adjacency list of the NGraph object.
It keeps the whole information of the link between a node u and its outgoing
neighbours: the index of the node v and the label of the arc linking u to v.
*/

class NEdge
{
  public :
    
    // =======================================================================
    //                                 Enums
    // =======================================================================
    
    // =======================================================================
    //                               Constructors
    // =======================================================================
    NEdge() = delete;
    NEdge( const NEdge& ) = default;
    NEdge( int node, string labels ) : m_node(node), m_labels(std::move(labels)) {}
    NEdge( int node, char label1, char label2 ) : m_node(node) {
      char buf[3] = { label1, label2, '\0' };
      m_labels = std::string( buf );
    }

    // =======================================================================
    //                            Accessors: getters
    // =======================================================================
    inline int    get_node( void ) const { return m_node; }
    inline string get_labels( void ) const { return m_labels; }
    

    // =======================================================================
    //                            Accessors: setters
    // =======================================================================
    inline void set_node( int node ) { m_node = node; }
    inline void set_labels( string labels ) { m_labels = std::move(labels); }
    

    // =======================================================================
    //                                Operators
    // =======================================================================

    // =======================================================================
    //                              Public Methods
    // =======================================================================
    void add_label( const string& label ) { m_labels += label; }
    void del_all_but_first() { m_labels.erase( 2 ); }
    void revert_dir( int i ) { m_labels[i] = reverse_dir(m_labels[i]); }

    // =======================================================================
    //                             Public Attributes
    // =======================================================================

  protected :
    // =======================================================================
    //                              Protected Methods
    // =======================================================================

    // =======================================================================
    //                             Protected Attributes
    // =======================================================================
    //! Index of the node 
    int m_node;
  
    //! Label of the incoming edge
    string m_labels;
};

#endif // NEDGE_H
