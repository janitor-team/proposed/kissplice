#!/usr/bin/perl -w

#break fasta file containing cycles into two fasta files
#file 1: upper paths of cycles
#file 2: lower paths of cycles

if(@ARGV !=3){
print "format:filter_coherence.pl inputfasta.pl upper.fa lower.fa \n";
print "Please specify the names of the input file and the output files\n"; 
exit 1;
}

$infile=shift;  #idio me $infile=$ARGV[0]
#idio me $outfile= $ARGV[1]

$outfile1=shift;
$outfile2=shift;

open(IN, "$infile") || die "Cannot open: $!\n";
open (OUT1,">$outfile1") || die "Cannot open: $!\n";
open(OUT2, ">$outfile2") || die "Cannot open: $!\n";

#>Cycle_31936_upper_Length:93_coverage1:24_coverage2:34
#ACCTTCCAGCTGGCGGTGGCTATCATTGATCGCTACCTGCAGGTGGTCAGGGACACCAAACGCACGTACTTGCAATTGGTGGGAGTGACAGCA
#>Cycle_31936_lower_Length:93_coverage1:23_coverage2:30
#ACCTTCCAGCTGGCGGTGGCTATCATTGATCGCTACCTGCAGGCGGTCAAGGACACCAAACGCACGTACTTGCAATTGGTGGGAGTGACAGCA


while($line=<IN>){
chomp($line);
if ($line=~m/^>Cycle_(\d+)_upper_(.*)/){

print  OUT1 "$line";
print OUT1 "\n";
$line=<IN>;
print  OUT1 "$line";
}

if ($line=~m/^>Cycle_(\d+)_lower_(.*)/){

print  OUT2 "$line";
print OUT2 "\n";
$line=<IN>;
print  OUT2 "$line";
}

}
