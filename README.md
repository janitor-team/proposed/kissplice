# KisSplice

Main page: http://kissplice.prabi.fr/

[User guide](https://gitlab.inria.fr/erable/kissplice/-/jobs/artifacts/master/file/prefix/share/doc/kissplice/user_guide.pdf?job=check_install_and_user_guide) from latest build.

# Pre-packaged versions

## Docker
You can find the latest version of KisSplice, KisSplice2RefGenome and KissDE [on Docker Hub](https://hub.docker.com/repository/docker/dwishsan/kissplice-pipeline).

We also propose a [stand-alone Docker image for the KissDE Shiny App](https://hub.docker.com/repository/docker/dwishsan/kde-shiny) for KisSplice results exploration.

## Linux
A package for kissplice is available on Debian / Ubuntu, but it may lag the current version.

# Build from source

## Dependencies
Required:
- cmake >= 3.1
- C/C++11 compiler toolchain (binutils, gcc or clang)
- python3 to run kissplice

Recommended but optional:
- [bcalm](https://github.com/GATB/bcalm) >= v2.2.2 ; as a fallback a locally compiled version of bcalm can be used by passing `-DUSE_BUNDLED_BCALM=TRUE` to cmake. A debian/ubuntu package is available. 
- latex toolchain with standard packages to build the user_guide ; this can be disabled by passing `-DUSER_GUIDE=OFF` to cmake

## Build
Download from the latest [release](https://gitlab.inria.fr/erable/kissplice/-/releases) and uncompress it.
You can also clone the git repository, but the latest code may be broken.
Then compile using :
```
cd kissplice/ # Replace by the directory where the release tar.gz was uncompressed, or where the git was cloned
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
make -j 4
```

After build the user guide (if not disabled) can be found in `build/doc/user_guide.pdf`.

The tool can be installed to a custom directory (like `~/.local/`) with:
```
cmake -DCMAKE_INSTALL_PREFIX=<directory> ..
make
make install
```

# Running on the sample test
```
cd kissplice/build
./bin/kissplice -r ../sample_example/mock1.fq -r ../sample_example/mock2.fq -r ../sample_example/virus1.fq -r ../sample_example/virus2.fq
```
